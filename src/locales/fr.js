export const fr = {
  ACTIONS: {
    ADD_ARTWORK: 'Ajouter une œuvre',
    ADD_EXHBITION: 'Ajouter une manifestation',
    RICH_TEXT_EDITOR: {
      MENTION_BUTTON: {
        PROJECTOUTPUT: "Mentionner un objet artistique",
      }
    }
  },
  ARTISTIC_OBJECT: {
    COLLECTION: 'Collection',
    COURTESY: 'Courtesy',
    CREATE: 'Nouvel objet artistique',
    CREDITS: 'Crédits',
    DESCRIPTION: 'Description',
    DIMENSIONS: 'Dimensions',
    IMAGES: "Images",
    NO_COLLECTION: 'Aucune',
    NO_COURTESY: 'Aucune',
    NO_CREDITS: 'Aucun',
    NO_DESCRIPTION: 'Aucune',
    NO_DIMENSIONS: 'Aucune',
    NO_IMAGE: 'Aucune image',
    NO_PROJECT: 'Aucun',
    NO_REPRESENTATION: 'Aucune',
    NO_TECHNICAL_INFO: 'Aucune',
    NO_YEAR: 'Aucune',
    PROJECTS: 'Projets associés',
    REPRESENTATIONS: 'Œuvres ou manifestations associées',
    SHORT_DESCRIPTION: 'Description résumée',
    TECHNICAL_INFO: 'Informations techniques',
    TITLE: 'Titre',
    SUBTITLE: 'Sous-titre',
    YEAR: 'Année',
    NEW: 'Ajouter une œuvre ou une exposition',
  },
  ARTWORK: {
    CREATE: 'Nouvelle œuvre',
    NEW: 'Ajouter une œuvre',
    SUB_LABEL:"Sélectionner une ou des œuvres à associer",
  },
  BREADCRUMB: {
    ARTWORKS: 'Liste des œuvres',
    EXHIBITIONS: 'Liste des manifestations',
  },
  EXHIBITION: {
    CREATE: 'Nouvelle manifestation',
    NEW: 'Ajouter une manifestation',
    SUB_LABEL:'Sélectionner une ou des manifestations à associer'
  },
  PROJECT: {
    ARTWORKS_HEADER: 'Œuvres associées',
    EXHBITIONS_HEADER: 'Manifestations associées',
    NO_RELATED_ARTWORKS: 'Aucune œuvre',
    NO_RELATED_EXHIBITION: 'Aucune manifestion',
    NO_RELATED_SUBPROJECTS: 'Aucun sous-projet',
  },
  SEARCH: {
    COLUMNS: {
      ARTISTIC_OBJECTS: {
        PICTURES_COUNT: 'Images associées',
      },
    },
    TABS: {
      ARTWORKS: 'Œuvres',
      EXHIBITIONS: 'Manifestations',
    },
  },
  TYPENAME:{
      ARTWORK: 'Œuvre',
      EXHIBITION: 'Manifestation',
  }
};
