export const en = {
  ACTIONS: {
    ADD_ARTWORK: 'Add an artwork',
    ADD_EXHBITION: 'Add an exhbition',
    RICH_TEXT_EDITOR: {
      MENTION_BUTTON: {
        PROJECTOUTPUT: "Mention an artistic object",
      }
    }
  },
  ARTISTIC_OBJECT: {
    COLLECTION: 'Collection',
    COURTESY: 'Courtesy',
    CREATE: 'New artistic object',
    CREDITS: 'Credits',
    DESCRIPTION: 'Description',
    DIMENSIONS: 'Dimensions',
    IMAGES: "Images",
    NO_COLLECTION: 'None',
    NO_COURTESY: 'None',
    NO_CREDITS: 'None',
    NO_DESCRIPTION: 'None',
    NO_DIMENSIONS: 'None',
    NO_IMAGE: 'No image',
    NO_PROJECT: 'None',
    NO_REPRESENTATION: 'None',
    NO_TECHNICAL_INFO: 'None',
    NO_YEAR: 'None',
    PROJECTS: 'Related projects',
    REPRESENTATIONS: 'Related artwork(s) or manifestation(s)',
    ARTWORKS: 'Related artwork(s) ',
    SHORT_DESCRIPTION: 'Short description',
    TECHNICAL_INFO: 'Technical informations',
    TITLE: 'Title',
    SUBTITLE: 'Subtitle',
    YEAR: 'Year',
    NEW: 'Add a new artwork or manifestation',
  },
  ARTWORK: {
    CREATE: 'New artwork',
    NEW: 'Add an artwork',
    SUB_LABEL:"Select one or more artworks to link",
  },
  BREADCRUMB: {
    ARTWORKS: 'List of artworks',
    EXHIBITIONS: 'List of exhibitions',
  },
  EXHIBITION: {
    CREATE: 'New exhibition',
    NEW: 'Add an exhibition',
    SUB_LABEL:'Select one or more exhibition to link'
  },
  PROJECT: {
    ARTWORKS_HEADER: 'Associated artworks',
    EXHBITIONS_HEADER: 'Associated exhibitions',
    NO_RELATED_ARTWORKS: 'No related artworks',
    NO_RELATED_EXHIBITION: 'No related exhibitions',
    NO_RELATED_SUBPROJECTS: 'No related sub-projects',
  },
  SEARCH: {
    COLUMNS: {
      ARTISTIC_OBJECTS: {
        PICTURES_COUNT: 'Pictures count',
      }
    },
    TABS: {
      ARTWORKS: 'Artworks',
      EXHIBITIONS: 'Exhibitions',
    },
  },
  TYPENAME:{
      ARTWORK: 'Artwork',
      EXHIBITION: 'Exhibition',
  }
};
