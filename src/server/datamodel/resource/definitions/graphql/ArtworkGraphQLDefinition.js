import {
  GraphQLTypeDefinition,
  getLinkedObjectsResolver,
  getLinkedObjectsCountResolver,
  generateConnectionArgs
} from "@mnemotix/synaptix.js";
import ProjectDefinition from "../ProjectDefinition";

export class ArtworkGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * Add extra GraphQL code specific to this model
   * @returns {String}
   */
  static getExtraGraphQLCode() {
    return `
extend type Project{
  """ Related artworks """
  artworks(${generateConnectionArgs()}): ArtworkConnection
  
  """ Count for related artworks """
  artworksCount(${generateConnectionArgs()}): Int
}
    `;
  }

  /**
   * Return extra resolvers to merge to the schema resolvers
   *
   * @returns {Object}
   */
  static getExtraResolvers() {
    return {
      Project: {
        artworks: getLinkedObjectsResolver.bind(
          this,
          ProjectDefinition.getLink("hasArtwork")
        ),
        artworksCount: getLinkedObjectsCountResolver.bind(
          this,
          ProjectDefinition.getLink("hasArtwork")
        )
      }
    };
  }
}
