import {
  GraphQLTypeDefinition,
  getLinkedObjectsResolver,
  getLinkedObjectsCountResolver,
  generateConnectionArgs
} from "@mnemotix/synaptix.js";
import ProjectDefinition from "../ProjectDefinition";

export class ExhibitionGraphQLDefinition extends GraphQLTypeDefinition {
  /**
   * Add extra GraphQL code specific to this model
   * @returns {String}
   */
  static getExtraGraphQLCode() {
    return `
extend type Project{
  """ Related exhibitions """
  exhibitions(${generateConnectionArgs()}): ExhibitionConnection
  
  """ Count for related exhibitions """
  exhibitionsCount(${generateConnectionArgs()}): Int
}
    `;
  }

  /**
   * Return extra resolvers to merge to the schema resolvers
   *
   * @returns {Object}
   */
  static getExtraResolvers() {
    return {
      Project: {
        exhibitions: getLinkedObjectsResolver.bind(
          this,
          ProjectDefinition.getLink("hasExhibition")
        ),
        exhibitionsCount: getLinkedObjectsCountResolver.bind(
          this,
          ProjectDefinition.getLink("hasExhibition")
        )
      }
    };
  }
}
