/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  ModelDefinitionAbstract,
  MnxOntologies,
  LabelDefinition,
  LiteralDefinition,
  LinkDefinition
} from "@mnemotix/synaptix.js";
import {ArtworkGraphQLDefinition} from "./graphql/ArtworkGraphQLDefinition";

export default class ArtworkDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition(){
    return ArtworkGraphQLDefinition;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MnxOntologies.mnxProject.ModelDefinitions.ProjectOutputDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "rs:Artwork";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return MnxOntologies.mnxProject.ModelDefinitions.ProjectOutputDefinition.getIndexType();
  }

  /**
   * @inheritDoc
   */
  static getIndexFilters(){
    // While action index is shared between Creation/Update/Deletion, we must filter types
    return [{
      "term":  {
        "types": "https://data.lafayetteanticipations.com/ontology/resource/Artwork"
      }
    }]
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasOwnerOrganization',
        rdfObjectProperty: "mnx:hasOwnerOrganization",
        relatedModelDefinition: MnxOntologies.mnxAgent.ModelDefinitions.OrganizationDefinition,
        isCascadingUpdated: true,
        relatedInputName: 'ownerOrganizationInput'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'year',
        rdfDataProperty: 'rs:year',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#integer",
      }),
      new LiteralDefinition({
        literalName: 'collection',
        rdfDataProperty: 'rs:collection',
      }),
      new LiteralDefinition({
        literalName: 'dimensions',
        rdfDataProperty: 'rs:dimensions',
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'courtesy',
        rdfDataProperty: 'rs:courtesy',
      }),
      new LabelDefinition({
        labelName: 'technicalInfo',
        rdfDataProperty: 'rs:technicalInfo',
      }),
      new LabelDefinition({
        labelName: 'credits',
        rdfDataProperty: 'rs:credits',
      })
    ];
  }
};