/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import {
  ModelDefinitionAbstract,
  MnxOntologies,
  LinkDefinition,
  GraphQLTypeDefinition
} from "@mnemotix/synaptix.js";
import ExhibitionDefinition from "./ExhibitionDefinition";
import ArtworkDefinition from "./ArtworkDefinition";

export default class ProjectDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   *  @inheritDoc
   */
  static substituteModelDefinition(){
    return MnxOntologies.mnxProject.ModelDefinitions.ProjectDefinition;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasArtwork',
        symmetricLinkName: "hasProject",
        rdfObjectProperty: "mnx:hasProjectOutput",
        relatedModelDefinition: ArtworkDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'artworkInputs'
      }),
      new LinkDefinition({
        linkName: 'hasExhibition',
        symmetricLinkName: "hasProject",
        rdfObjectProperty: "mnx:hasProjectOutput",
        relatedModelDefinition: ExhibitionDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'exhibitionInputs'
      }),
    ];
  }
};