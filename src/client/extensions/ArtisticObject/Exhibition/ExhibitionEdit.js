/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {useParams, useNavigate} from "react-router-dom";
import {useMutation, useLazyQuery} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";
import {useSnackbar} from "notistack";

import {ROUTES} from "../routes";
import {LoadingSplashScreen, createLink} from "@mnemotix/weever-core";

import {ArtisticObjectDynamicForm} from "../Common/ArtisticObjectDynamicForm";
import {ExhibitionFormContent} from "./ExhibitionFormContent";

import {gqlExhibition} from "./gql/Exhibition.gql";
import {gqlUpdateExhibition} from "./gql/UpdateExhibition.gql";
import {gqlCreateExhibition} from "./gql/CreateExhibition.gql";
import {exhibitionFormDefinition} from "./form/Exhibition.form";

export default function ExhibitionEdit({id} = {}) {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const params = useParams();
  const {enqueueSnackbar} = useSnackbar();

  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [getExhibition, {data: {exhibition} = {}, loading} = {}] = useLazyQuery(gqlExhibition);

  useEffect(() => {
    if (id) {
      getExhibition({
        variables: {id}
      });
    }
  }, [id]);

  const [mutateExhibition, {loading: saving}] = useMutation(id ? gqlUpdateExhibition : gqlCreateExhibition, {
    onCompleted(data) {
      if (id) {
        navigate(formatRoute(ROUTES.EXHIBITION, {id}));
      } else {
        id = data.createExhibition.createdObject.id;
        navigate(formatRoute(ROUTES.EXHIBITION, {id}));
      }

      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <ArtisticObjectDynamicForm
      formDefinition={exhibitionFormDefinition}
      artisticObject={exhibition}
      mutateFunction={mutateExhibition}
      saving={saving}
      extraBreadcrumbComponent={createLink({to: ROUTES.EXHIBITIONS, text: t("BREADCRUMB.EXHIBITIONS")})}
      ArtisticObjectFormContent={ExhibitionFormContent}
    />
  );
}
