import {object, date, string} from "yup";
import {DynamicFormDefinition, MutationConfig} from "@mnemotix/weever-core";

import {gqlExhibitionFragment} from "../gql/Exhibition.gql";

export function getExhibitionValidationSchema({t}) {
  return object().shape({
    title: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    subTitle: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    startDate: date().nullable().default(undefined),
    endDate: date().nullable().default(undefined),
  });
}

export const exhibitionFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["title", "description", "shortDescription", "startDate", "endDate", "subTitle"],
    postProcessInitialValues: object => ({
      ...object,
      startDate: object.startDate === "" ? null : object.startDate, // Replace "" by null to make DatePickerField working
      endDate: object.endDate === "" ? null : object.endDate // Replace "" by null to make DatePickerField working
    }),
    gqlFragment: gqlExhibitionFragment,
    gqlFragmentName: "ExhibitionFragment"
  }),
  validationSchema: getExhibitionValidationSchema
});
