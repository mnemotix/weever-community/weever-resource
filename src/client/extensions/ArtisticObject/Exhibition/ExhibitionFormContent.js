import {Grid} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {DatePickerField, TextField} from "@mnemotix/weever-core";
import {ArtisticObjectFormContent} from "../Common/ArtisticObjectFormContent";

export function ExhibitionFormContent({id}) {
  const {t} = useTranslation();

  return (
    <ArtisticObjectFormContent
      id={id}
      ExtraFormContent={
        <>
          <Grid item xs={12} md={6}>
            <DatePickerField name="startDate" label={t("PROJECT_CONTRIBUTION.START_DATE")} />
          </Grid>
          <Grid item xs={12} md={6}>
            <DatePickerField name="endDate" label={t("PROJECT_CONTRIBUTION.END_DATE")} />
          </Grid>
        </>
      }
      ExtraFormHeaderContent={
        <Grid item xs={12}>
          <TextField required name="subTitle" label={t("ARTISTIC_OBJECT.SUBTITLE")} />
        </Grid>
      }
    />
  );
}
