/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../routes";
import uniq from "lodash/uniq";

import {Avatar} from "@material-ui/core";
import {
  CollectionView,
  createLink,
  EnforcedButton,
  FloatingButton,
  generateCollectionViewGenericColumns,
  getImageThumbnail,
  useLoggedUser,
  useResponsive
} from "@mnemotix/weever-core";

import {makeStyles} from "@material-ui/core/styles";
import LocalPlayIcon from "@material-ui/icons/LocalPlay";
import {gqlExhibitions} from "./gql/Exhibitions.gql";

const useStyles = makeStyles((theme) => ({
  image: {
    textDecoration: "none"
  },
  imageCell: {
    width: theme.spacing(8)
  },
  title: {
    textDecoration: "none",
    color: theme
  },
  grow: {
    flexGrow: 1
  }
}));

export default function Exhibitions({qs} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();
  const {isContributor, isEditor} = useLoggedUser();
  const {isDesktop} = useResponsive();

  const columns = [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    {
      name: "pictureUrls",
      label: " ",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (pictureUrls, {row}) => {
          return (
            <Link className={classes.image} to={formatRoute(ROUTES.EXHIBITION, {id: row.id})}>
              <Avatar
                src={getImageThumbnail({
                  imageUrl: pictureUrls?.[0],
                  size: "small"
                })}>
                <LocalPlayIcon />
              </Avatar>
            </Link>
          );
        },
        setCellProps: () => ({
          sx: classes.imageCell
        })
      }
    },
    {
      name: "title",
      label: t("SEARCH.COLUMNS.COMMON.TITLE"),
      // displayAsMainTitle: true,
      options: {
        filter: false,
        sort: true,
        customBodyRender: (title, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.EXHIBITION, {id: row.id}),
            text: title
          });
        }
      }
    },
    {
      name: "projectTitles",
      label: t("SEARCH.COLUMNS.COMMON.PROJECT"),
      options: {
        customBodyRender: (projectTitles) => {
          return uniq(projectTitles || []).join(" / ");
        }
      }
    },
    ...generateCollectionViewGenericColumns({t})
  ];

  return (
    <CollectionView
      columns={columns}
      gqlConnectionPath={"exhibitions"}
      gqlCountPath={"exhibitionsCount"}
      gqlQuery={gqlExhibitions}
      qs={qs}
      availableDisplayModes={{table: {}}}
      renderRightSideActions={() => {
        return isDesktop ? (
          <EnforcedButton locked={!isContributor} to={formatRoute(ROUTES.EXHIBITION_CREATE)}>
            {t("EXHIBITION.CREATE")}
          </EnforcedButton>
        ) : (
          <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.EXHIBITION_CREATE)} name="AddIcon" />
        );
      }}
      removalEnabled={false}
    />
  );
}
