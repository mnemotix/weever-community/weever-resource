/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useQuery} from "@apollo/client";
import {LoadingSplashScreen, ExcerptGenericRender} from "@mnemotix/weever-core";

import {gqlExhibition} from "./gql/Exhibition.gql";

export function ExhibitionExcerpt({id}) {
  const {data, loading} = useQuery(gqlExhibition, {
    variables: {
      id
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <ExcerptGenericRender
      picture={data?.exhibition?.pictures?.edges?.[0]?.node?.publicUrl}
      title={data?.exhibition?.title}
      shortDescription={data?.exhibition?.shortDescription}
      createdAt={data?.exhibition?.createdAt}
      displayName={data?.exhibition?.creatorPerson?.fullName}
    />
  );
}
