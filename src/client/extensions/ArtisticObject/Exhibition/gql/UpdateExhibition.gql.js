import {gql} from "@apollo/client";

export const gqlUpdateExhibition = gql`
  mutation UpdateExhibition($input: UpdateExhibitionInput!) {
    updateExhibition(input: $input) {
      updatedObject {
        id
      }
    }
  }
`;
