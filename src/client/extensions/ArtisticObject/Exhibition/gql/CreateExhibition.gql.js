import {gql} from "@apollo/client";

export const gqlCreateExhibition = gql`
  mutation createExhibition($input: CreateExhibitionInput!) {
    createExhibition(input: $input) {
      createdObject {
        id
      }
    }
  }
`;
