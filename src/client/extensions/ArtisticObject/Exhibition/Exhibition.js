/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useParams} from "react-router-dom";
import {useQuery} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {Grid, Typography} from "@material-ui/core";
import {createLink, RichTextViewer, TranslatableDisplay} from "@mnemotix/weever-core";
import {ArtisticObject} from "../Common/ArtisticObject";
import {gqlExhibition} from "./gql/Exhibition.gql";
import {ROUTES} from "../routes";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  image: {
    width: theme.spacing(35),
    height: theme.spacing(35),
    backgroundPosition: "center",
    backgroundSize: "cover"
  },
  breadcrumbs: {
    marginBottom: theme.spacing(2)
  },
  toolbar: {
    marginTop: theme.spacing(2),
    textAlign: "right"
  },
  empty: {
    color: theme.palette.text.disabled
  }
}));

export default function Exhibition({} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();

  let {id} = useParams();
  id = decodeURIComponent(id);

  const {data, loading} = useQuery(gqlExhibition, {
    variables: {
      id
    }
  });

  return (
    <ArtisticObject
      addDates={true}
      loading={loading}
      artisticObject={data?.exhibition}
      extraBreadcrumbComponent={createLink({to: ROUTES.EXHIBITIONS, text: t("BREADCRUMB.EXHIBITIONS")})}
      extraHeadingComponent={(
        <Grid item xs={12}>
          <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.SUBTITLE")}</Typography>

          <div className={clsx({[classes.empty]: !data?.exhibition.subTitle})}>
            <TranslatableDisplay isTranslated={data?.exhibition.subTitleTranslated}>
              <RichTextViewer content={data?.exhibition.subTitle || t("ARTISTIC_OBJECT.NO_DESCRIPTION")} />
            </TranslatableDisplay>
          </div>
        </Grid>
      )}
    />
  );
}
