
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ROUTES as COMMON_ROUTES} from "@mnemotix/weever-core";

export const ROUTES = {
  ...COMMON_ROUTES,
  ARTWORKS: "/artworks",
  ARTWORK: "/artworks/:id",
  ARTWORK_EDIT: "/artworks/edit/:id",
  ARTWORK_CREATE: "/artworks/create/new",
  EXHIBITIONS: "/exhibitions",
  EXHIBITION: "/exhibitions/:id",
  EXHIBITION_EDIT: "/exhibitions/edit/:id",
  EXHIBITION_CREATE: "/exhibitions/create/new",
  EXPLORE_ARTWORK: "/explore/artworks/:id"
};