/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useQuery} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../routes";
import {createLink, ErrorBoundary, ExcerptPopover, globalStyles, LoadingSplashScreen} from "@mnemotix/weever-core";
import {ArtworkExcerpt} from "../Artwork/ArtworkExcerpt";
import {gqlProjectArtworks} from "./gql/ProjectArtworks.gql";

export function ProjectArtworks(props) {
  return (
    <ErrorBoundary>
      <ProjectArtworksCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectArtworksCode({projectId} = {}) {
  const {t} = useTranslation();
  const globalClasses = globalStyles();
  let data,
    loading = false;

  if (projectId) {
    ({data, loading} = useQuery(gqlProjectArtworks, {
      variables: {
        projectId
      }
    }));
  }

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <Choose>
      <When condition={data?.project.artworks.edges.length > 0}>
        {data?.project.artworks.edges.map(({node: artwork}) => (
          <span className={globalClasses.commaAfter} key={artwork.id}>
            <ExcerptPopover OnHoverDisplayComponent={<ArtworkExcerpt id={artwork.id} />}>
              {createLink({
                text: artwork.title,
                to: formatRoute(ROUTES.ARTWORK, {id: artwork.id})
              })}
            </ExcerptPopover>
          </span>
        ))}
      </When>
      <Otherwise>
        <div className={globalClasses.empty}>{t("PROJECT.NO_RELATED_ARTWORKS")}</div>
      </Otherwise>
    </Choose>
  );
}
