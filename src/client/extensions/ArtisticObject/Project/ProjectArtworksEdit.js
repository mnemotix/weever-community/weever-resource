/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";

import {ListItemText} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {LoadingSplashScreen, PluralLinkField, ErrorBoundary} from "@mnemotix/weever-core";
import {ArtworkFormContent} from "../Artwork/ArtworkFormContent";
import {ArtworkAutocomplete} from "../Artwork/ArtworkAutocomplete";
import {projectArtworksInputDefinition} from "./form/ProjectArtwork.form";
import {gqlProjectArtworks} from "./gql/ProjectArtworks.gql";

export function ProjectArtworksEdit(props) {
  return (
    <ErrorBoundary>
      <ProjectArtworksEditCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectArtworksEditCode({projectId} = {}) {
  const {t} = useTranslation();

  const [getProjectArtworks, {data: {project} = {}, loading}] = useLazyQuery(gqlProjectArtworks);
  useEffect(() => {
    if (projectId) {
      getProjectArtworks({
        variables: {
          projectId
        }
      });
    }
  }, [projectId]);

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <PluralLinkField
      data={project}
      linkInputDefinition={projectArtworksInputDefinition}
      renderObjectContent={(artwork) => {
        return <ListItemText primary={artwork?.title} />;
      }}
      renderObjectAutocomplete={({...props}) => {
        return <ArtworkAutocomplete {...props} />;
      }}
      renderObjectForm={() => <ArtworkFormContent />}
      addButtonLabel={t("ARTWORK.NEW")}
    />
  );
}
