import {LinkInputDefinition} from "@mnemotix/weever-core";
import {exhibitionFormDefinition} from "../../Exhibition/form/Exhibition.form";

export const projectExhibitionLinkInputDefinition = new LinkInputDefinition({
  name: "exhibitions",
  isPlural: true,
  inputName: "exhibitionInputs",
  targetObjectFormDefinition: exhibitionFormDefinition,
  forceUpdateTarget: false
});
