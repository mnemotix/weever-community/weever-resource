import {LinkInputDefinition} from "@mnemotix/weever-core";
import {artworkFormDefinition} from "../../Artwork/form/Artwork.form";

export const projectArtworksInputDefinition = new LinkInputDefinition({
  name: "artworks",
  isPlural: true,
  inputName: "artworkInputs",
  targetObjectFormDefinition: artworkFormDefinition,
  forceUpdateTarget: false
});

