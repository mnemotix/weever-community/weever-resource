import {gql} from "@apollo/client";
import {gqlExhibitionFragment} from "../../Exhibition/gql/Exhibition.gql";

export const gqlProjectExhibitions = gql`
  query ProjectExhibitions($projectId: ID!) {
    project(id: $projectId) {
      id
      exhibitions {
        edges {
          node {
            ...ExhibitionFragment
          }
        } 
      }
    }
  }
	${gqlExhibitionFragment}
`;
