/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";

import {ExhibitionExcerpt} from "../Exhibition/ExhibitionExcerpt";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../routes";
import {createLink, LoadingSplashScreen, globalStyles, ErrorBoundary, ExcerptPopover} from "@mnemotix/weever-core";
import {useQuery} from "@apollo/client";
import {gqlProjectExhibitions} from "./gql/ProjectExhibitions.gql";

export function ProjectExhibitions(props) {
  return (
    <ErrorBoundary>
      <ProjectExhibitionsCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectExhibitionsCode({projectId} = {}) {
  const {t} = useTranslation();
  const globalClasses = globalStyles();

  let data,
    loading = false;

  if (projectId) {
    ({data, loading} = useQuery(gqlProjectExhibitions, {
      variables: {projectId}
    }));
  }

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <Choose>
      <When condition={data?.project.exhibitions.edges.length > 0}>
        {data?.project.exhibitions.edges.map(({node: exhibition}) => (
          <span className={globalClasses.commaAfter} key={exhibition.id}>
            <ExcerptPopover OnHoverDisplayComponent={<ExhibitionExcerpt id={exhibition.id} />}>
              {createLink({
                text: exhibition.title,
                to: formatRoute(ROUTES.EXHIBITION, {id: exhibition.id})
              })}
            </ExcerptPopover>
          </span>
        ))}
      </When>
      <Otherwise>
        <div className={globalClasses.empty}>{t("PROJECT.NO_RELATED_EXHIBITION")}</div>
      </Otherwise>
    </Choose>
  );
}
