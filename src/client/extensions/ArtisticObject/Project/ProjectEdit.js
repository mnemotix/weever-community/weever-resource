/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import Grid from "@material-ui/core/Grid";

import {ShowWhereIAM} from "@mnemotix/weever-core";
import {ProjectArtworksEdit} from "./ProjectArtworksEdit";
import {ProjectExhibitionsEdit} from "./ProjectExhibitionsEdit";

const useStyles = makeStyles((theme) => ({
  heading: {
    marginTop: theme.spacing(4)
  },
  gutterBottom: {
    marginBottom: theme.spacing(2)
  }
}));

export function ProjectEdit({projectId}) {
  const classes = useStyles();
  const {t} = useTranslation();

  return (
    <ShowWhereIAM path="src/client/extensions/ArtisticObject/Project/ProjectEdit.js weever">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle1">{t("PROJECT.ARTWORKS_HEADER")}</Typography>

          <div className={classes.gutterBottom}>
            <ProjectArtworksEdit projectId={projectId} />
          </div>

          <Typography variant="subtitle1">{t("PROJECT.EXHBITIONS_HEADER")}</Typography>

          <div className={classes.gutterBottom}>
            <ProjectExhibitionsEdit projectId={projectId} />
          </div>
        </Grid>
      </Grid>
    </ShowWhereIAM>
  );
}
