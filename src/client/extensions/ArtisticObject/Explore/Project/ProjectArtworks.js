/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../routes";
import {ErrorBoundary, LoadingSplashScreen, CategoryTitle} from "@mnemotix/weever-core";
import {gqlProjectArtworks} from "./gql/ProjectArtworks.gql";
import {useTranslation} from "react-i18next";

export function ProjectArtworks(props) {
  return (
    <ErrorBoundary>
      <ProjectArtworksCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectArtworksCode({projectId} = {}) {
  const {t} = useTranslation();
  let data,
    loading = false;

  if (projectId) {
    ({data, loading} = useQuery(gqlProjectArtworks, {
      variables: {
        projectId
      }
    }));
  }

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (data?.project.artworks.edges.length < 1) {
    return null;
  }


  function generateItemRoute(node) {
    return formatRoute(ROUTES.ARTWORK, {id: node.id}) //EXPLORE_ARTWORK
  }

  const _artworks = data?.project.artworks.edges.map((item) => item.node)
  return (<>
    <CategoryTitle title={t("PROJECT.ARTWORKS_HEADER")} />

    <ItemsInRow data={_artworks} uniqkey={"ProjectArtworks"} generateItemRoute={generateItemRoute} md={4} lg={4} compactMode={true}
      hideImage={true}
      dataForView={(node) => {
        const {pictures, ...props} = node;
        return {image: pictures?.edges?.node?.publicUrl?.edges, ...props};
      }}
    />
  </>
  )
}
