import {gql} from "@apollo/client";


export const gqlProjectExhibitions = gql`
  query ProjectExhibitions($projectId: ID!) {
    project(id: $projectId) {
      id
      exhibitions {
        edges {
          node {
            id
            title
          }
        }
      }
    }
  }
`;

/*
export const gqlProjectExhibitions = gql`
  query ProjectExhibitions($projectId: ID!) {
    project(id: $projectId) {
      id
      exhibitions {
        edges {
          node {
            id
            title
            description 
            shortDescription 
            pictures {
              edges {
                node {
                  id
                  publicUrl
                }
              }
            }
          }
        }
      }
    }
  }
`;
 */