/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Tab from "@material-ui/core/Tab";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {ROUTES} from "../routes";

import {gqlSearch} from "./gql/Search.gql";
import {useNavigate} from "react-router-dom";

export const SearchTabsSettings = [
  {
    route: ROUTES.ARTWORKS,
    generateLabel: ({t, data}) => t("SEARCH.TABS.ARTWORKS") + (data?.artworksCount ? ` (${data.artworksCount})` : "")
  },
  {
    route: ROUTES.EXHIBITIONS,
    generateLabel: ({t, data}) =>
      t("SEARCH.TABS.EXHIBITIONS") + (data?.exhibitionsCount ? ` (${data.exhibitionsCount})` : "")
  }
];

/**
 * @return {*}
 */
export function SearchTabs({qs, fullWidth} = {}) {
  const {t} = useTranslation();
  let navigate = useNavigate();

  const {data} = useQuery(gqlSearch, {
    variables: {
      qs
    }
  });

  return SearchTabsSettings.map(({generateLabel, route}, key) => (
    <Tab
      key={key}
      value={key}
      label={generateLabel({t, data})}
      fullWidth={fullWidth}
      onClick={(e) => navigate(route)}
    />
  ));
}
