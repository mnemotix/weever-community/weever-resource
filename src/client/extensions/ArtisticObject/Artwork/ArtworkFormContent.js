import {Grid} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {NumberField, TranslatableField, TextField} from "@mnemotix/weever-core";
import {ArtisticObjectFormContent} from "../Common/ArtisticObjectFormContent";

export function ArtworkFormContent({id}) {
  const {t} = useTranslation();

  return (
    <ArtisticObjectFormContent
      id={id}
      ExtraFormContent={
        <>
          <Grid item xs={12}>
            <NumberField name="year" id="year" label={t("ARTISTIC_OBJECT.YEAR")} />
          </Grid>

          <Grid item xs={12}>
            <TextField name="dimensions" label={t("ARTISTIC_OBJECT.DIMENSIONS")} />
          </Grid>
          <Grid item xs={12}>
            <TranslatableField
              FieldComponent={TextField}
              name="technicalInfo"
              label={t("ARTISTIC_OBJECT.TECHNICAL_INFO")}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField name="collection" label={t("ARTISTIC_OBJECT.COLLECTION")} />
          </Grid>
          <Grid item xs={12}>
            <TranslatableField FieldComponent={TextField} name="courtesy" label={t("ARTISTIC_OBJECT.COURTESY")} />
          </Grid>
          <Grid item xs={12}>
            <TranslatableField FieldComponent={TextField} name="credits" label={t("ARTISTIC_OBJECT.CREDITS")} />
          </Grid>
        </>
      }
    />
  );
}
