/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {
  CollectionView,
  createLink,
  generateCollectionViewGenericColumns,
  getImageThumbnail,
  useLoggedUser,
  EnforcedButton,
  FloatingButton,
  useResponsive
} from "@mnemotix/weever-core";
import uniq from "lodash/uniq";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../routes";
import {Avatar} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import FilterVintageIcon from "@material-ui/icons/FilterVintage";
import {gqlArtworks} from "./gql/Artworks.gql";

const useStyles = makeStyles((theme) => ({
  image: {
    textDecoration: "none"
  },
  imageCell: {
    width: theme.spacing(8)
  },
  title: {
    textDecoration: "none",
    color: theme
  },
  grow: {
    flexGrow: 1
  }
}));

export default function Artworks({qs} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();
  const {isContributor, isEditor} = useLoggedUser();
  const {isDesktop} = useResponsive();

  const columns = [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    {
      name: "pictureUrls",
      label: " ",
      options: {
        filter: false,
        sort: false,
        customBodyRender: (pictureUrls, {row}) => {
          return (
            <Link className={classes.image} to={formatRoute(ROUTES.ARTWORK, {id: row.id})}>
              <Avatar
                src={getImageThumbnail({
                  imageUrl: pictureUrls?.[0],
                  size: "small"
                })}>
                <FilterVintageIcon />
              </Avatar>
            </Link>
          );
        },
        setCellProps: () => ({
          sx: classes.imageCell
        })
      }
    },
    {
      name: "title",
      label: t("SEARCH.COLUMNS.COMMON.TITLE"),
      // displayAsMainTitle: true,
      options: {
        filter: false,
        sort: true,
        customBodyRender: (title, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ARTWORK, {id: row.id}),
            text: title
          });
        }
      }
    },
    {
      name: "projectTitles",
      label: t("SEARCH.COLUMNS.COMMON.PROJECT"),
      options: {
        customBodyRender: (projectTitles) => {
          return uniq(projectTitles || []).join(" / ");
        }
      }
    },
    ...generateCollectionViewGenericColumns({t})
  ];

  return (
    <CollectionView
      columns={columns}
      gqlConnectionPath={"artworks"}
      gqlCountPath={"artworksCount"}
      gqlQuery={gqlArtworks}
      qs={qs}
      availableDisplayModes={{table: {}}}
      renderRightSideActions={() => {
        return isDesktop ? (
          <EnforcedButton locked={!isContributor} to={formatRoute(ROUTES.ARTWORK_CREATE)}>
            {t("ARTWORK.CREATE")}
          </EnforcedButton>
        ) : (
          <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.ARTWORK_CREATE)} name="AddIcon" />
        );
      }}
      removalEnabled={false}
    />
  );
}
