/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useParams} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {SanitizedHtml} from "@mnemotix/synaptix-client-toolkit";
import clsx from "clsx";

import {ROUTES} from "../routes";
import {TranslatableDisplay, createLink} from "@mnemotix/weever-core";
import {ArtisticObject} from "../Common/ArtisticObject";
import {gqlArtwork} from "./gql/Artwork.gql";

const useStyles = makeStyles((theme) => ({
  image: {
    width: theme.spacing(35),
    height: theme.spacing(35),
    backgroundPosition: "center",
    backgroundSize: "cover"
  },
  breadcrumbs: {
    marginBottom: theme.spacing(2)
  },
  toolbar: {
    marginTop: theme.spacing(2),
    textAlign: "right"
  },
  empty: {
    color: theme.palette.text.disabled
  }
}));

export default function Artwork({} = {}) {
  const classes = useStyles();
  const {t} = useTranslation();
  let {id} = useParams();
  id = decodeURIComponent(id);

  const {data, loading, fetchMore} = useQuery(gqlArtwork, {
    variables: {
      id
    }
  });

  return (
    <ArtisticObject
      loading={loading}
      artisticObject={data?.artwork}
      extraBreadcrumbComponent={createLink({to: ROUTES.ARTWORKS, text: t("BREADCRUMB.ARTWORKS")})}
      extraFootingComponent={
        data && (
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.YEAR")}</Typography>

              <div className={clsx({[classes.empty]: !data?.artwork?.year})}>
                {data?.artwork?.year || t("ARTISTIC_OBJECT.NO_YEAR")}
              </div>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.DIMENSIONS")}</Typography>

              <div className={clsx({[classes.empty]: !data?.artwork?.dimensions})}>
                {data?.artwork?.dimensions || t("ARTISTIC_OBJECT.NO_DIMENSIONS")}
              </div>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.TECHNICAL_INFO")}</Typography>

              <div className={clsx({[classes.empty]: !data?.artwork?.technicalInfo})}>
                <TranslatableDisplay isTranslated={data?.artwork?.technicalInfoTranslated}>
                  <SanitizedHtml html={data?.artwork?.technicalInfo || t("ARTISTIC_OBJECT.NO_TECHNICAL_INFO")} />
                </TranslatableDisplay>
              </div>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.COLLECTION")}</Typography>

              <div className={clsx({[classes.empty]: !data?.artwork?.collection})}>
                {data?.artwork?.collection || t("ARTISTIC_OBJECT.NO_COLLECTION")}
              </div>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.COURTESY")}</Typography>

              <div className={clsx({[classes.empty]: !data?.artwork?.courtesy})}>
                <TranslatableDisplay isTranslated={data?.artwork?.courtesyTranslated}>
                  <SanitizedHtml html={data?.artwork?.courtesy || t("ARTISTIC_OBJECT.NO_COURTESY")} />
                </TranslatableDisplay>
              </div>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.CREDITS")}</Typography>

              <div className={clsx({[classes.empty]: !data?.artwork?.credits})}>
                <TranslatableDisplay isTranslated={data?.artwork?.creditsTranslated}>
                  <SanitizedHtml html={data?.artwork?.credits || t("ARTISTIC_OBJECT.NO_CREDITS")} />
                </TranslatableDisplay>
              </div>
            </Grid>
          </Grid>
        )
      }
    />
  );
}
