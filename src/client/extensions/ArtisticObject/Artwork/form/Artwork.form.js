import {object, string} from "yup";
import {DynamicFormDefinition, MutationConfig} from "@mnemotix/weever-core";

import {gqlArtworkFragment} from "../gql/Artwork.gql";

export function getArtworkValidationSchema({t}) {
  return object().shape({
    title: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED"))
  });
}

export const artworkFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: [
      "title",
      "description",
      "shortDescription",
      "collection",
      "courtesy",
      "credits",
      "technicalInfo",
      "dimensions",
      "year"
    ],
    gqlFragment: gqlArtworkFragment,
    gqlFragmentName: "ArtworkFragment"
  }),
  validationSchema: getArtworkValidationSchema
});
