/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useQuery} from "@apollo/client";
import {LoadingSplashScreen, ExcerptGenericRender} from "@mnemotix/weever-core";
import {useTranslation} from "react-i18next";

import {gqlArtwork} from "./gql/Artwork.gql";

export function ArtworkExcerpt({id}) {
  const {t} = useTranslation();

  const {data, loading} = useQuery(gqlArtwork, {
    variables: {
      id
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <ExcerptGenericRender
      picture={data?.artwork?.pictures?.edges?.[0]?.node?.publicUrl}
      title={data?.artwork?.title}
      shortDescription={data?.artwork?.shortDescription}
      createdAt={data?.artwork?.createdAt}
      displayName={data?.artwork?.creatorPerson?.fullName}
    />
  );
}
