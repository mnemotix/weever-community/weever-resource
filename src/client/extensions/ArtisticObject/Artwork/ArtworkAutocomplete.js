import {gql} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {GenericAutocomplete} from "@mnemotix/weever-core";

const gqlArtworks = gql`
  query Artworks($qs: String, $first: Int, $filters: [String], $sortings: [SortingInput]) {
    artworks(qs: $qs, first: $first, sortings: $sortings, filters: $filters) {
      edges {
        node {
          id
          title
        }
      }
    }
  }
`;

/**
 * @param {function} onSelect
 * @param {string} placeholder
 * @param {object} selectedObject
 * @param {string} error
 * @param {boolean} creationEnabled
 * @param {function} onCreate
 */
export function ArtworkAutocomplete({onSelect, placeholder, selectedObject, error, creationEnabled, onCreate} = {}) {
  const {t} = useTranslation();
  return (
    <GenericAutocomplete
      placeholder={placeholder || t("ARTWORK.AUTOCOMPLETE.CHOOSE")}
      gqlEntitiesQuery={gqlArtworks}
      gqlEntitiesConnectionPath={"artworks"}
      gqlEntityLabelPath={"title"}
      onSelect={onSelect}
      selectedObject={selectedObject}
      creationEnabled={creationEnabled}
      onCreate={onCreate}
      AutocompleteProps={{
        noOptionsText: t("ARTWORK.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
    />
  );
}
