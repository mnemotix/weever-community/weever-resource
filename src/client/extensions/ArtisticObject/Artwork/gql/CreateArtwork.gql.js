import {gql} from "@apollo/client";

export const gqlCreateArtwork = gql`
  mutation CreateArtwork($input: CreateArtworkInput!) {
    createArtwork(input: $input) {
      createdObject {
        id
      }
    }
  }
`;
