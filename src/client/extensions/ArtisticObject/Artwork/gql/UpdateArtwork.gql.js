import {gql} from "@apollo/client";

export const gqlUpdateArtwork = gql`
  mutation UpdateArtwork($input: UpdateArtworkInput!) {
    updateArtwork(input: $input) {
      updatedObject {
        id
      }
    }
  }
`;
