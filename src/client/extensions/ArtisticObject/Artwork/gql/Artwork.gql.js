/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {gql} from "@apollo/client";
import {gqlArtisticObjectBasicProps, gqlArtisticObjectFragment} from "../../Common/gql/ArtisticObject.gql";

export const gqlArtworkBasicProps = [
  ...gqlArtisticObjectBasicProps,
  "dimensions",
  "year",
  "collection",
  "courtesy",
  "credits",
  "technicalInfo"
];

export const gqlArtworkFragment = gql`
  fragment ArtworkFragment on Artwork {
    ...ArtisticObjectFragment
    dimensions
    year
    collection
    courtesy
    courtesyTranslated
    credits
    creditsTranslated
    technicalInfo
    technicalInfoTranslated
  }

  ${gqlArtisticObjectFragment}
`;

export const gqlArtwork = gql`
  query Artwork($id: ID!) {
    artwork(id: $id) {
      id
      ...ArtworkFragment
      createdAt
      creatorPerson {
        id
        fullName
      }
    }
  }

  ${gqlArtworkFragment}
`;
