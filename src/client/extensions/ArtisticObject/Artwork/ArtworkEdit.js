/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {useMutation, useLazyQuery} from "@apollo/client";
import {useParams, useNavigate} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../routes";

import {ShowWhereIAM, createLink, LoadingSplashScreen} from "@mnemotix/weever-core";
import {ArtisticObjectDynamicForm} from "../Common/ArtisticObjectDynamicForm";
import {ArtworkFormContent} from "./ArtworkFormContent";

import {gqlArtwork} from "./gql/Artwork.gql";
import {gqlUpdateArtwork} from "./gql/UpdateArtwork.gql";
import {gqlCreateArtwork} from "./gql/CreateArtwork.gql";
import {useSnackbar} from "notistack";

import {artworkFormDefinition} from "./form/Artwork.form";

export default function ArtworkEdit({id} = {}) {
  const navigate = useNavigate();
  const {enqueueSnackbar} = useSnackbar();
  const {t} = useTranslation();
  const params = useParams();

  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [getArtwork, {data: {artwork} = {}, loading} = {}] = useLazyQuery(gqlArtwork);

  useEffect(() => {
    if (id) {
      getArtwork({
        variables: {id}
      });
    }
  }, [id]);

  // mutation
  const [mutateArtwork, {loading: saving}] = useMutation(id ? gqlUpdateArtwork : gqlCreateArtwork, {
    onCompleted(data) {
      if (id) {
        navigate(formatRoute(ROUTES.ARTWORK, {id}));
      } else {
        id = data?.createArtwork.createdObject.id;
        navigate(formatRoute(ROUTES.ARTWORK, {id}));
      }
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <ShowWhereIAM title="ArtworkEdit" path="src/client/extensions/ArtisticObject/Artwork/ArtworkEdit.js">
      <ArtisticObjectDynamicForm
        formDefinition={artworkFormDefinition}
        artisticObject={artwork}
        mutateFunction={mutateArtwork}
        saving={saving}
        extraBreadcrumbComponent={createLink({to: ROUTES.ARTWORKS, text: t("BREADCRUMB.ARTWORKS")})}
        ArtisticObjectFormContent={ArtworkFormContent}
      />
    </ShowWhereIAM>
  );
}
