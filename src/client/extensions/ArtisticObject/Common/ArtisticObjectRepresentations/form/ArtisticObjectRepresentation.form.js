/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {LinkInputDefinition, MixedLinkInputDefinition} from "@mnemotix/weever-core";
import {artworkFormDefinition} from "../../../Artwork/form/Artwork.form";
import {exhibitionFormDefinition} from "../../../Exhibition/form/Exhibition.form";

export const artisticObjectRepresentationsInputDefinition = new MixedLinkInputDefinition({
  name: "representations",
  isPlural: true,
  inputName: "representationInputs",
  mixedLinks: [
    new LinkInputDefinition({
      targetObjectFormDefinition: artworkFormDefinition,
      inputInheritedTypename: "Artwork",
    }),
    new LinkInputDefinition({
      targetObjectFormDefinition: exhibitionFormDefinition,
      inputInheritedTypename: "Exhibition",
    })
  ]
});