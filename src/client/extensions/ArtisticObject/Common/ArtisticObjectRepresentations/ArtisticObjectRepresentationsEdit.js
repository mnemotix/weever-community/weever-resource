/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {useLazyQuery} from "@apollo/client";
import {Grid, ListItemText, Typography, TextField as Select, MenuItem} from "@material-ui/core";

import {useTranslation} from "react-i18next";

import {ErrorBoundary, LoadingSplashScreen, PluralLinkField, ShowWhereIAM} from "@mnemotix/weever-core";

import {ExhibitionFormContent} from "../../Exhibition/ExhibitionFormContent";
import {ArtworkFormContent} from "../../Artwork/ArtworkFormContent";
import {ArtworkAutocomplete} from "../../Artwork/ArtworkAutocomplete";
import {ExhitionAutocomplete} from "../../Exhibition/ExhitionAutocomplete";

import {artisticObjectRepresentationsInputDefinition} from "./form/ArtisticObjectRepresentation.form";
import {gqlArtisticObjectRepresentations} from "./gql/ArtisticObjectRepresentations.gql";

export function ArtisticObjectRepresentationsEdit(props) {
  return (
    <ErrorBoundary>
      <ArtisticObjectRelatedArtisticObjectsEditCode {...props} />
    </ErrorBoundary>
  );
}

function ArtisticObjectRelatedArtisticObjectsEditCode({artisticObjectId} = {}) {
  const {t} = useTranslation();

  const [getRelatedArtisticObjects, {data, loading}] = useLazyQuery(gqlArtisticObjectRepresentations);

  useEffect(() => {
    if (artisticObjectId) {
      getRelatedArtisticObjects({
        variables: {
          id: artisticObjectId
        }
      });
    }
  }, [artisticObjectId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <ShowWhereIAM
      title="ArtisticObjectRelatedArtisticObjectsEdit"
      path="/extensions/Project/artisticObjects/ArtisticObjectRelatedArtisticObjectsEdit">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.REPRESENTATIONS")}</Typography>

          <PluralLinkField
            data={data?.artisticObject}
            linkInputDefinition={artisticObjectRepresentationsInputDefinition}
            linkInputInheritedTypename={"Artwork"}
            renderObjectContent={(artisticObject) => {
              return <ListItemText primary={artisticObject?.title} />;
            }}
            renderObjectAutocomplete={({linkInputInheritedTypename, setLinkInputInheritedTypename, ...props}) => {
              return (
                <Grid container spacing={2}>
                  <Grid item xs={12} md={3}>
                    <Select
                      select
                      fullWidth
                      value={linkInputInheritedTypename}
                      onChange={(event) => setLinkInputInheritedTypename(event.target.value)}
                      label={t("AGENT.AFFILIATION.AGENT_TYPE")}>
                      <MenuItem value={"Artwork"}>{t("TYPENAME.ARTWORK")}</MenuItem>
                      <MenuItem value={"Exhibition"}>{t("TYPENAME.EXHIBITION")}</MenuItem>
                    </Select>
                  </Grid>
                  <Grid item xs={12} md={9}>
                    {linkInputInheritedTypename === "Artwork" ? (
                      <ArtworkAutocomplete {...props} />
                    ) : (
                      <ExhitionAutocomplete {...props} />
                    )}
                  </Grid>
                </Grid>
              );
            }}
            renderObjectForm={({linkInputInheritedTypename}) => {
              return linkInputInheritedTypename === "Artwork" ? <ArtworkFormContent /> : <ExhibitionFormContent />;
            }}
            addButtonLabel={t("ARTISTIC_OBJECT.NEW")}
          />
        </Grid>
      </Grid>
    </ShowWhereIAM>
  );
}
