/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {createLink, ErrorBoundary, ExcerptPopover, globalStyles} from "@mnemotix/weever-core";
import {ArtworkExcerpt} from "../../Artwork/ArtworkExcerpt";
import {ExhibitionExcerpt} from "../../Exhibition/ExhibitionExcerpt";

import {ROUTES} from "../../routes";
import {gqlArtisticObjectRepresentations} from "./gql/ArtisticObjectRepresentations.gql";

export function ArtisticObjectRepresentations(props) {
  return (
    <ErrorBoundary>
      <ArtisticObjectRelatedArtisticObjectsCode {...props} />
    </ErrorBoundary>
  );
}

function ArtisticObjectRelatedArtisticObjectsCode({artisticObjectId} = {}) {
  // invariant(artisticObjectId, "artisticObjectId is required");
  const globalClasses = globalStyles();
  const {t} = useTranslation();

  const {data, loading, fetchMore} = useQuery(gqlArtisticObjectRepresentations, {
    variables: {
      id: artisticObjectId
    }
  });

  return (
    !loading && (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.REPRESENTATIONS")}</Typography>

          <Choose>
            <When condition={data?.artisticObject.representations.edges.length > 0}>
              {data.artisticObject.representations.edges.map(({node: artisticObject}) => (
                <span key={artisticObject.id} className={globalClasses.commaAfter}>
                  <ExcerptPopover
                    OnHoverDisplayComponent={
                      artisticObject.__typename === "Artwork" ? (
                        <ArtworkExcerpt id={artisticObject.id} />
                      ) : (
                        <ExhibitionExcerpt id={artisticObject.id} />
                      )
                    }>
                    {createLink({
                      to: formatRoute(artisticObject.__typename === "Artwork" ? ROUTES.ARTWORK : ROUTES.EXHIBITION, {
                        id: artisticObject.id
                      }),
                      text: artisticObject.title
                    })}
                  </ExcerptPopover>
                </span>
              ))}
            </When>
            <Otherwise>
              <div className={globalClasses.empty}>{t("ARTISTIC_OBJECT.NO_REPRESENTATION")}</div>
            </Otherwise>
          </Choose>
        </Grid>
      </Grid>
    )
  );
}
