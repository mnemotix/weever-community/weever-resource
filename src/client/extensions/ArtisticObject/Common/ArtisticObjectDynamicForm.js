import {instanceOf, func, bool, object} from "prop-types";
import {makeStyles} from "@material-ui/core/styles";
import {Typography, Breadcrumbs, Grid} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {TaggingsField, DynamicForm, DynamicFormDefinition} from "@mnemotix/weever-core";

import {ArtisticObjectAuthorsEdit} from "./ArtisticObjectAuthors/ArtisticObjectAuthorsEdit";
import {ArtisticObjectProjectsEdit} from "./ArtisticObjectProjects/ArtisticObjectProjectsEdit";
import {ArtisticObjectRepresentationsEdit} from "./ArtisticObjectRepresentations/ArtisticObjectRepresentationsEdit";

const useStyles = makeStyles((theme) => ({
  breadcrumbs: {
    marginBottom: theme.spacing(2)
  }
}));

/**
 * @param {DynamicFormDefinition} formDefinition
 * @param {object} artisticObject
 * @param {function} mutateFunction
 * @param {boolean} saving
 * @param {function} extraBreadcrumbComponent
 * @param {function|JSX.Element} ArtisticObjectFormContent
 * @return {JSX.Element}
 */
export function ArtisticObjectDynamicForm({
  formDefinition,
  artisticObject,
  mutateFunction,
  saving,
  extraBreadcrumbComponent,
  ArtisticObjectFormContent
} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();

  return (
    <>
      <Breadcrumbs className={classes.breadcrumbs}>
        {extraBreadcrumbComponent}
        <Typography color="textPrimary">{artisticObject?.title || t("ARTISTIC_OBJECT.CREATE")}</Typography>
      </Breadcrumbs>
      <DynamicForm
        object={artisticObject}
        formDefinition={formDefinition}
        mutateFunction={mutateFunction}
        saving={saving}>
        <Grid container spacing={2}>
          <ArtisticObjectFormContent id={artisticObject?.id} />
          <Grid item xs={12}>
            <ArtisticObjectAuthorsEdit artisticObjectId={artisticObject?.id} />
          </Grid>
          <Grid item xs={12}>
            <ArtisticObjectProjectsEdit artisticObjectId={artisticObject?.id} />
          </Grid>
          <Grid item xs={12}>
            <ArtisticObjectRepresentationsEdit artisticObjectId={artisticObject?.id} />
          </Grid>
          <Grid item xs={12}>
            <TaggingsField entityId={artisticObject?.id} />
          </Grid>
        </Grid>
      </DynamicForm>
    </>
  );
}

ArtisticObjectDynamicForm.propTypes = {
  formDefinition: instanceOf(DynamicFormDefinition).isRequired,
  mutateFunction: func.isRequired,
  ArtisticObjectFormContent: func.isRequired,
  saving: bool,
  artisticObject: object
};
