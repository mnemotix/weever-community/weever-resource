/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import {useTranslation} from "react-i18next";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";
import clsx from "clsx";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";

import {Link} from "react-router-dom";

import {ROUTES} from "../routes";
import {
  createLink,
  EnforcedButton,
  EntityAccessPolicy,
  ErrorBoundary,
  getImageThumbnail,
  LoadingSplashScreen,
  RichTextViewer,
  Taggings,
  TranslatableDisplay,
  useLoggedUser,
  FloatingButton,
  useResponsive
} from "@mnemotix/weever-core";

import {ArtisticObjectProjects} from "./ArtisticObjectProjects/ArtisticObjectProjects";
import {ArtisticObjectAuthors} from "./ArtisticObjectAuthors/ArtisticObjectAuthors";
import {ArtisticObjectRepresentations} from "./ArtisticObjectRepresentations/ArtisticObjectRepresentations";

const useStyles = makeStyles((theme) => ({
  image: {
    width: theme.spacing(35),
    height: theme.spacing(35),
    backgroundPosition: "center",
    backgroundSize: "cover"
  },
  breadcrumbs: {
    marginBottom: theme.spacing(2)
  },
  toolbar: {
    marginTop: theme.spacing(2),
    textAlign: "right"
  },
  empty: {
    color: theme.palette.text.disabled
  },
  noImage: {
    width: "100%",
    color: theme.palette.text.disabled,
    padding: [[theme.spacing(15), 0]],
    textAlign: "center",
    backgroundColor: theme.palette.grey[200]
  }
}));

export function ArtisticObject(props) {
  return (
    <ErrorBoundary>
      <ArtisticObjectCode {...props} />
    </ErrorBoundary>
  );
}

function ArtisticObjectCode({
  addDates,
  loading,
  artisticObject,
  extraBreadcrumbComponent,
  extraHeadingComponent,
  extraFootingComponent
} = {}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const {isContributor} = useLoggedUser();
  const {isDesktop} = useResponsive();
  let editRouteName =
    artisticObject && artisticObject["__typename"] === "Exhibition" ? ROUTES.EXHIBITION_EDIT : ROUTES.ARTWORK_EDIT;

  let tiles = getPictureTiles();

  return loading || !artisticObject ? (
    <LoadingSplashScreen />
  ) : (
    <>
      <Breadcrumbs className={classes.breadcrumbs}>
        {extraBreadcrumbComponent}
        <Typography color="textPrimary">{artisticObject.title}</Typography>
      </Breadcrumbs>
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h5" gutterBottom>
                <TranslatableDisplay isTranslated={artisticObject.titleTranslated}>
                  {artisticObject.title}
                </TranslatableDisplay>
              </Typography>
            </Grid>

            <If condition={extraHeadingComponent}>
              <Grid item xs={12}>
                {extraHeadingComponent}
              </Grid>
            </If>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.SHORT_DESCRIPTION")}</Typography>

              <div className={clsx({[classes.empty]: !artisticObject.shortDescription})}>
                <TranslatableDisplay isTranslated={artisticObject.shortDescriptionTranslated}>
                  <RichTextViewer content={artisticObject.shortDescription || t("ARTISTIC_OBJECT.NO_DESCRIPTION")} />
                </TranslatableDisplay>
              </div>
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.DESCRIPTION")}</Typography>

              <div className={clsx({[classes.empty]: !artisticObject.description})}>
                <TranslatableDisplay isTranslated={artisticObject.descriptionTranslated}>
                  <RichTextViewer content={artisticObject.description || t("ARTISTIC_OBJECT.NO_DESCRIPTION")} />
                </TranslatableDisplay>
              </div>
            </Grid>

            {!!addDates && (
              <>
                <Grid item xs={12} md={6}>
                  <Typography variant="subtitle1">{t("PROJECT_CONTRIBUTION.START_DATE")}</Typography>
                  <div
                    className={clsx({
                      [classes.noDate]: !artisticObject.startDate
                    })}>
                    {artisticObject.startDate
                      ? dayjs(artisticObject.startDate).format("DD/MM/YYYY")
                      : t("PROJECT_CONTRIBUTION.NO_DATE")}
                  </div>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Typography variant="subtitle1">{t("PROJECT_CONTRIBUTION.END_DATE")}</Typography>
                  <div
                    className={clsx({
                      [classes.noDate]: !artisticObject.endDate
                    })}>
                    {artisticObject.endDate
                      ? dayjs(artisticObject.endDate).format("DD/MM/YYYY")
                      : t("PROJECT_CONTRIBUTION.NO_DATE")}
                  </div>
                </Grid>
              </>
            )}

            <If condition={extraFootingComponent}>
              <Grid item xs={12}>
                {extraFootingComponent}
              </Grid>
            </If>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ENTITY.ACCESS_POLICY")}</Typography>
              <EntityAccessPolicy entityId={artisticObject.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ENTITY.TAGGINGS")}</Typography>
              <Taggings entityId={artisticObject.id} />
            </Grid>

            <Grid item xs={12}>
              <ArtisticObjectAuthors artisticObjectId={artisticObject.id} />
            </Grid>

            <Grid item xs={12}>
              <ArtisticObjectProjects artisticObjectId={artisticObject.id} />
            </Grid>

            <Grid item xs={12}>
              <ArtisticObjectRepresentations artisticObjectId={artisticObject.id} />
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12} md={4}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Choose>
                <When condition={tiles.length > 0}>
                  <GridList cellHeight={160}>
                    {tiles.map(({picture, featured, expanded}) => (
                      <GridListTile key={picture.id} cols={expanded || featured ? 2 : 1} rows={featured ? 2 : 1}>
                        <img
                          src={getImageThumbnail({
                            imageUrl: picture.publicUrl,
                            size: "medium"
                          })}
                          alt={""}
                        />
                      </GridListTile>
                    ))}
                  </GridList>
                </When>
                <Otherwise>
                  <div className={classes.noImage}>{t("ARTISTIC_OBJECT.NO_IMAGE")}</div>
                </Otherwise>
              </Choose>
            </Grid>

            <Grid item xs={6}>
              <div>{t("ENTITY.CREATED_AT_BY", {date: dayjs(artisticObject.createdAt).format("LL")})}</div>
              <div>
                {createLink({
                  to: formatRoute(ROUTES.PERSON, {id: artisticObject?.creatorPerson?.id}),
                  text: artisticObject?.creatorPerson?.fullName
                })}
              </div>
            </Grid>
            {isDesktop && (
              <Grid item xs={6} align="right" className={classes.actions}>
                <EnforcedButton
                  locked={!isContributor}
                  variant="contained"
                  color="primary"
                  component={Link}
                  to={formatRoute(editRouteName, {id: artisticObject.id})}>
                  {t("ACTIONS.UPDATE")}
                </EnforcedButton>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
      {!isDesktop && (
        <FloatingButton
          locked={!isContributor}
          to={formatRoute(editRouteName, {id: artisticObject.id})}
          name="EditIcon"
        />
      )}
    </>
  );

  function getPictureTiles() {
    let tiles = [];
    let expandLastOne = artisticObject?.pictures?.edges?.length || 0 % 2 === 0;

    for (let [index, {node: picture}] of artisticObject?.pictures?.edges?.entries() || []) {
      tiles.push({
        picture,
        featured: index === 0,
        expanded: index === (artisticObject.pictures?.edges?.length || 0) - 1 && expandLastOne
      });
    }

    return tiles;
  }
}
