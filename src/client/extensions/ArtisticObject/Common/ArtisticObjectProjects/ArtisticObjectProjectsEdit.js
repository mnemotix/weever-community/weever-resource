/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {Grid, ListItemText, Typography} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {
  ErrorBoundary,
  PluralLinkField,
  ProjectFormContent,
  ProjectsAutocomplete,
  AccessPolicyField
} from "@mnemotix/weever-core";

import {gqlArtisticObjectProjects} from "./gql/ArtisticObjectProjects.gql";
import {artisticObjectProjectLinkInputDefinition} from "./form/ArtisticObjectProject.form";

export function ArtisticObjectProjectsEdit(props) {
  return (
    <ErrorBoundary>
      <ArtisticObjectProjectsEditCode {...props} />
    </ErrorBoundary>
  );
}
function ArtisticObjectProjectsEditCode({artisticObjectId} = {}) {
  const {t} = useTranslation();

  const [getArtisticObjectProjects, {data, loading}] = useLazyQuery(gqlArtisticObjectProjects);

  useEffect(() => {
    if (artisticObjectId) {
      getArtisticObjectProjects({
        variables: {
          id: artisticObjectId
        }
      });
    }
  }, [artisticObjectId]);

  return (
    !loading && (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.PROJECTS")}</Typography>
          <PluralLinkField
            data={data?.artisticObject}
            linkInputDefinition={artisticObjectProjectLinkInputDefinition}
            renderObjectContent={(project) => {
              return <ListItemText primary={project.title} />;
            }}
            renderObjectForm={() => (
              <>
                <ProjectFormContent />
                <AccessPolicyField entityId={artisticObjectId} />
              </>
            )}
            renderObjectAutocomplete={({...props}) => {
              return <ProjectsAutocomplete {...props} />;
            }}
            addButtonLabel={t("PROJECT.NEW")}
          />
        </Grid>
      </Grid>
    )
  );
}
