/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../routes";
import {createLink, MentionDefinition} from "@mnemotix/weever-core";
import {ArtisticObjectsMentionPopper} from "./ArtisticObjectsMentionPopper";

/**
 * @type {MentionDefinition}
 */
export const ArtisticObjectsMentionDefinition = new MentionDefinition({
  triggerChar: "&",
  type: "ProjectOutput",
  renderMentionsPopper: ({mentionText, onSelectMention}) => (
    <ArtisticObjectsMentionPopper qs={mentionText} onSelect={onSelectMention} />
  ),
  getMentionText: (artisticObject) => artisticObject.title,
  getMentionAttributes: (artisticObject) => ({
    id: artisticObject.id,
    typename: artisticObject.__typename
  }),
  renderMentionElement: ({element, readOnly, children}) => {
    if (readOnly) {
      return createLink({
        to: formatRoute(element.mentionAttributes.typename === "Artwork" ? ROUTES.ARTWORK : ROUTES.EXHIBITION, {
          id: element.mentionAttributes.id
        }),
        children: (
          <>
            {element.mentionText}
            {children}
          </>
        ),
        contentEditable: false,
        sx: {userSelect: "none"}
      });
    }
  }
});
