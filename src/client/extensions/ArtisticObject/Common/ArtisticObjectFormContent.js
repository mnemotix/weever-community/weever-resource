/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Grid} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import * as Yup from "yup";

import {
  TextField,
  ResourcePickerField,
  TranslatableField,
  AccessPolicyField,
  RichTextAreaField
} from "@mnemotix/weever-core";
import {artisticObjectPicturesInputDefinition} from "./ArtisticObjectPictures/form/ArtisticObjectPictures.form";

export const formikValidationSchema = Yup.object().shape({
  title: Yup.string().required("Required"),
  description: Yup.string(),
  shortDescription: Yup.string().min(2, "Too Short!")
});

/**
 *
 * @param {string} id
 * @param {boolean} addDates : add or not startDate & endDate
 */
export function ArtisticObjectFormContent({id, ExtraFormContent, ExtraFormHeaderContent} = {}) {
  const {t} = useTranslation();

  return (
    <Grid item container spacing={3}>
      <Grid item xs={12}>
        <TranslatableField FieldComponent={TextField} required={true} name="title" label={t("ARTISTIC_OBJECT.TITLE")} />
      </Grid>

      {ExtraFormHeaderContent}

      <Grid item xs={12}>
        <TranslatableField
          FieldComponent={RichTextAreaField}
          name="shortDescription"
          label={t("ARTISTIC_OBJECT.SHORT_DESCRIPTION")}
        />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="description" label={t("PROJECT.DESCRIPTION")} />
      </Grid>

      {ExtraFormContent}

      <Grid item xs={12}>
        <ResourcePickerField
          multiple
          label={t("ARTISTIC_OBJECT.IMAGES")}
          linkInputDefinition={artisticObjectPicturesInputDefinition}
        />
      </Grid>

      <Grid item xs={12}>
        <AccessPolicyField entityId={id} />
      </Grid>
    </Grid>
  );
}
