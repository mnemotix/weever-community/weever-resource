/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {makeStyles} from "@material-ui/core/styles";
import {Avatar, ListItemIcon, Typography} from "@material-ui/core";
import ArtworkIcon from "@material-ui/icons/FilterVintage";
import ExhibitionIcon from "@material-ui/icons/LocalPlay";

import {MentionPopper, getImageThumbnail} from "@mnemotix/weever-core";

import {gqlArtisticObjects} from "./gql/ArtisticObjects.gql";

const useStyles = makeStyles((theme) => ({
  avatar: {
    height: theme.spacing(2),
    width: theme.spacing(2),
    fontSize: 0.5 * theme.typography.fontSize,
    "& > .MuiSvgIcon-root": {
      height: theme.spacing(1.8),
      width: theme.spacing(1.8)
    }
  },
  listItemIcon: {
    minWidth: theme.spacing(2),
    marginRight: theme.spacing(1)
  }
}));

/**
 * @param {string} qs
 * @param {function} onSelect
 */
export function ArtisticObjectsMentionPopper({qs, onSelect} = {}) {
  const classes = useStyles();

  return (
    <MentionPopper
      qs={qs}
      gqlEntitiesQuery={gqlArtisticObjects}
      gqlEntitiesConnectionPath={"projectOutputs"}
      gqlVariables={{
        first: 10
      }}
      renderEntity={(projectOutput) => (
        <>
          <ListItemIcon className={classes.listItemIcon}>
            <Avatar
              src={getImageThumbnail({
                imageUrl: projectOutput.pictures?.[0]?.node.publicUrl,
                size: "small"
              })}
              className={classes.avatar}>
              <Choose>
                <When condition={projectOutput.__typename === "Artwork"}>
                  <ArtworkIcon />
                </When>
                <Otherwise>
                  <ExhibitionIcon />
                </Otherwise>
              </Choose>
            </Avatar>
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            {projectOutput.title}
          </Typography>
        </>
      )}
      onSelect={onSelect}
    />
  );
}
