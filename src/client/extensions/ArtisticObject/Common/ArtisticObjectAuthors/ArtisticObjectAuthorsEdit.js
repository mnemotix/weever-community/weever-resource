/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useEffect} from "react";

import {ListItemText, Grid, Typography, TextField as Select, MenuItem} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";

import {
  ErrorBoundary,
  PluralLinkField,
  PersonFormContent,
  PersonsAutocomplete,
  OrganizationFormContent,
  OrganizationsAutocomplete,
  LoadingSplashScreen
} from "@mnemotix/weever-core";

import {artisticObjectAuthorLinkInputDefinition} from "./form/ArtisticObjectAuthor.form";
import {gqlArtisticObjectAuthors} from "./gql/ArtisticObjectAuthors.gql";

/**
 * display and edit the authors related to an artistic objects
 * @param {*} props
 */
export function ArtisticObjectAuthorsEdit(props) {
  return (
    <ErrorBoundary>
      <ArtisticObjectAuthorsEditCode {...props} />
    </ErrorBoundary>
  );
}

function ArtisticObjectAuthorsEditCode({artisticObjectId} = {}) {
  const {t} = useTranslation();

  const [getArtisticObjectAuthors, {data : {artisticObject} = {}, loading} = {}] = useLazyQuery(gqlArtisticObjectAuthors);

  useEffect(() => {
    if (artisticObjectId) {
      getArtisticObjectAuthors({
        variables: {
          id: artisticObjectId
        }
      });
    }
  }, [artisticObjectId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.AUTHORS")}</Typography>
        <PluralLinkField
          data={artisticObject}
          linkInputDefinition={artisticObjectAuthorLinkInputDefinition}
          linkInputInheritedTypename={"Person"}
          renderObjectContent={agent => {
            return (
              <ListItemText
                primary={agent.__typename === "Person" ? [agent.firstName, agent.lastName].join(" ") : agent.name}
              />
            );
          }}
          renderObjectAutocomplete={({linkInputInheritedTypename, setLinkInputInheritedTypename, ...props}) => {
            return (
              <Grid container spacing={2}>
                <Grid item xs={12} md={3}>
                  <Select
                    select
                    fullWidth
                    value={linkInputInheritedTypename}
                    onChange={event => setLinkInputInheritedTypename(event.target.value)}
                    label={t("AGENT.AFFILIATION.AGENT_TYPE")}>
                    <MenuItem value={"Person"}>{t("TYPENAME.PERSON")}</MenuItem>
                    <MenuItem value={"Organization"}>{t("TYPENAME.ORGANIZATION")}</MenuItem>
                  </Select>
                </Grid>
                <Grid item xs={12} md={9}>
                  {linkInputInheritedTypename === "Person" ? (
                    <PersonsAutocomplete {...props} />
                  ) : (
                    <OrganizationsAutocomplete {...props} />
                  )}
                </Grid>
              </Grid>
            );
          }}
          renderObjectForm={({linkInputInheritedTypename}) =>
            linkInputInheritedTypename === "Person" ? <PersonFormContent /> : <OrganizationFormContent />
          }
          addButtonLabel={t("ACTIONS.ADD_AGENT")}
        />
      </Grid>
    </Grid>
  );
}
