/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import invariant from "invariant";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {createLink, ErrorBoundary, ExcerptPopover, globalStyles, PersonExcerpt} from "@mnemotix/weever-core";
import {ROUTES} from "../../routes";
import {gqlArtisticObjectAuthors} from "./gql/ArtisticObjectAuthors.gql";

/**
 * display related authors of artistic object
 * @param {*} props
 */
export function ArtisticObjectAuthors(props) {
  return (
    <ErrorBoundary>
      <ArtisticObjectAuthorsCode {...props} />
    </ErrorBoundary>
  );
}

function ArtisticObjectAuthorsCode({artisticObjectId} = {}) {
  invariant(artisticObjectId, "artisticObjectId is required");

  const globalClasses = globalStyles();
  const {t} = useTranslation();

  const {data, loading, fetchMore} = useQuery(gqlArtisticObjectAuthors, {
    variables: {
      id: artisticObjectId
    }
  });

  return (
    !loading && (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="subtitle1">{t("ARTISTIC_OBJECT.AUTHORS")}</Typography>

          <Choose>
            <When condition={data?.artisticObject?.authors?.edges?.length > 0}>
              {data?.artisticObject?.authors?.edges.map(({node: agent}) => (
                <span key={agent.id} className={globalClasses.commaAfter}>
                  <ExcerptPopover OnHoverDisplayComponent={<PersonExcerpt id={agent?.id} />}>
                    {createLink({
                      to: formatRoute(ROUTES.PERSON, {id: agent.id}),
                      text: agent.__typename === "Person" ? [agent.firstName, agent.lastName].join(" ") : agent.name
                    })}
                  </ExcerptPopover>
                </span>
              ))}
            </When>
            <Otherwise>
              <div className={globalClasses.empty}>{t("ARTISTIC_OBJECT.NO_AUTHORS")}</div>
            </Otherwise>
          </Choose>
        </Grid>
      </Grid>
    )
  );
}
