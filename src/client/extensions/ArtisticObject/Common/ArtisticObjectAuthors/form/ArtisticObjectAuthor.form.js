import {
  LinkInputDefinition,
  MixedLinkInputDefinition,
  personFormDefinition,
  organizationFormDefinition
} from "@mnemotix/weever-core";
/**
 * @type {LinkInputDefinition}
 */
export const artisticObjectAuthorLinkInputDefinition = new MixedLinkInputDefinition({
  name: "authors",
  inputName: "authorInputs",
  isPlural: true,
  mixedLinks: [
    new LinkInputDefinition({
      targetObjectFormDefinition: personFormDefinition,
      inputInheritedTypename: "Person"
    }),
    new LinkInputDefinition({
      targetObjectFormDefinition: organizationFormDefinition,
      inputInheritedTypename: "Organization"
    })
  ]
});
