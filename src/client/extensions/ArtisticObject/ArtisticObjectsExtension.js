/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Extension} from "@mnemotix/weever-core";
import {Route} from "react-router-dom";
import {ROUTES} from "./routes";
import loadable from "@loadable/component";

/**
 * Routes are code splitted
 */
const Artwork = loadable(() => import("./Artwork/Artwork"));
const ArtworkEdit = loadable(() => import("./Artwork/ArtworkEdit"));
const Exhibition = loadable(() => import("./Exhibition/Exhibition"));
const ExhibitionEdit = loadable(() => import("./Exhibition/ExhibitionEdit"));

import {SearchTabs, SearchTabsSettings} from "./Search/SearchTabs";
import {SearchRoutes} from "./Search/SearchRoutes";
import {Project} from "./Project/Project";
import {ProjectEdit} from "./Project/ProjectEdit";

export const artisticObjectsExtension = new Extension({
  name: "ArtisticObjects",
  generateTopRoutes: ({isContributor} = {}) => (
    <>
      <Route path={ROUTES.ARTWORK} element={<Artwork />} />
      <Route path={ROUTES.EXHIBITION} element={<Exhibition />} />

      <If condition={isContributor}>
        <Route path={ROUTES.ARTWORK_CREATE} element={<ArtworkEdit />} />
        <Route path={ROUTES.ARTWORK_EDIT} element={<ArtworkEdit />} />
        <Route path={ROUTES.EXHIBITION_CREATE} element={<ExhibitionEdit />} />
        <Route path={ROUTES.EXHIBITION_EDIT} element={<ExhibitionEdit />} />
      </If>
    </>
  ),
  fragments: {
    Search: {
      Tabs: SearchTabs,
      Routes: SearchRoutes
    },
    "Project.Heading.End": Project,
    ProjectEdit: ProjectEdit
  },
  settings: {
    Search: {
      SearchTabsSettings
    }
  }
});
