/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createMuiTheme} from "@material-ui/core/styles";
import AnticipationsSansOTF from "./assets/AnticipationsSans.ttf";

const mainColor = "#474747";
const secColor = "#d7d6d6";
const linkColor = "#9e9e9e";

const AnticipationsSansFont = {
  fontFamily: "AnticipationsSans",
  fontStyle: "normal",
  fontDisplay: "swap",
  fontWeight: 400,
  src: `url(${AnticipationsSansOTF}) format('woff2') `
};

export const theme = createMuiTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: mainColor
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: secColor
    },
    // Used by `getContrastText()` to maximize the contrast between
    // the background and the text.
    contrastThreshold: 3,
    // Used by the functions below to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
    text: {
      emptyHint: mainColor
    }
  },
  typography: {
    subtitle1: {
      fontSize: "1rem"
    }
  },
  overrides: {
    MuiTimelineItem: {
      missingOppositeContent: {
        "&:before": {
          display: "none"
        }
      }
    },
    MuiCssBaseline: {
      "@global": {
        "@font-face": [AnticipationsSansFont],
        a: {
          color: linkColor
        }
      }
    },
    MuiAppBar: {
      "@global": {
        ".MuiButton-textSecondary": {
          color: "white"
        }
      }
    },
    MuiLink: {
      root: {
        color: linkColor
      }
    },
    MuiSpeedDialAction: {
      staticTooltipLabel: {
        minWidth: "50vw"
      }
    },
    MuiBackdrop: {
      root: {
        zIndex: 0
      }
    },
    MuiInputBase: {
      root: {
        fontSize: "0.875rem",
        lineHeight: 1.43
      }
    },
    MuiInputLabel: {
      shrink: {
        transform: "translate(0, -4px)"
      }
    },
    MuiFormLabel: {
      root: {
        color: "inherit",
        fontSize: "1rem"
      }
    }
  }
});
